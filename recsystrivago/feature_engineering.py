import pandas as pd
import numpy as np
import sklearn as sk

import recsystrivago.utils as utils
import recsystrivago.ml_utils as ml_utils
import recsystrivago.constants as constants


def fe_display_row(display, d_label_encoder=None):
    """
    Label encode cols_to_le
    """
    display['current_filters_count'] = display['current_filters'].str.split('|').str.len()

    cols_to_le = ['search_type']

    if d_label_encoder is None:
        # Means we are fitting + transform (in train)
        d_label_encoder = {}
        for col in cols_to_le:
            le = sk.preprocessing.LabelEncoder()
            le.fit(display[col].fillna(''))
            d_label_encoder[col] = le
            display['{}_le'.format(col)] = le.transform(display[col].fillna(''))
    else:
        # Means we simply transform
        for col in cols_to_le:
            le = d_label_encoder[col]
            display['{}_le'.format(col)] = le.transform(display[col].fillna(''))
    return display, d_label_encoder


def get_fe_item_position(display, display_test):
    """
    Mean position of a given item.
    If item is ranked high, it might be because Trivago's team has idea why.
    Basicly we are using their predictions about an item.
    """
    fe_item_position = pd.concat([display, display_test], sort=False)
    group_col = ['item_id']
    group_name = '_&_'.join(c.upper() for c in group_col) + '_'
    fe_item_position = (
        fe_item_position.
        drop_duplicates(['session_id', 'item_id'], keep='first')
        .groupby(group_col).agg({'position': ['mean', 'count']}))

    fe_item_position.columns = ['_'.join(c) for c in fe_item_position]
    fe_item_position = fe_item_position.add_prefix(group_name)
    fe_item_position = fe_item_position.reset_index()
    return fe_item_position


def get_fe_item_position_next_day(display, display_test):
    """
    If trivago teams ranks an item higher the next day, it is probably because we had a click for the current day

    After data-analysis (not model training), this has 0 signals
    """
    position_day = pd.concat([display, display_test], sort=False)
    group_col = ['date', 'item_id']
    group_name = '_&_'.join(c.upper() for c in group_col) + '_'
    position_day = (
        position_day.
        drop_duplicates(['session_id', 'item_id'], keep='first')
        .groupby(group_col).agg({'position': ['mean', 'count']}))

    position_day.columns = ['_'.join(c) for c in position_day]
    position_next_day = position_day.add_prefix(group_name).add_suffix('_1')
    position_day = position_day.add_prefix(group_name)
    position_day = position_day.reset_index()
    position_next_day = position_next_day.reset_index()
    position_next_day['date'] = position_next_day['date'] + pd.DateOffset(days=-1)

    position_day = pd.merge(position_day, position_next_day)
    # If > 0, position are lost
    # If < 0, position are won
    position_day['DATE_&_ITEM_ID_position_mean_diff'] = (
        position_day['DATE_&_ITEM_ID_position_mean_1'] - position_day['DATE_&_ITEM_ID_position_mean'])

    return position_day


def get_item_ctr(display, viewed):
    """
    item's CTR out of fold by user.
    Rq : We prefer to not merge here user_fold, item_ctr here before it would
    explose number of rows.
    In get_learning() just merge user_fold then item_ctr.
    """
    user_fold = display[['user_id']].drop_duplicates()
    user_fold['user_fold'] = np.random.randint(0, 20, user_fold.shape[0])
    clicked = display[display['target'] == 1][['user_id', 'item_id', 'step', 'target']]
    # Last clickout step
    user_step_max = (
        clicked.groupby('user_id')['step']
        .agg('max').rename('user_step_max').reset_index())
    clicked = clicked.groupby(['user_id', 'item_id'])['target'].max().reset_index()
    # Smae shift as for viewed
    user_step_max['user_step_max'] += 1

    # We call noisy ctr as item_id which we are not sure they were seen
    ctr_noisy = viewed.copy()
    ctr_noisy['previously_viewed'] = np.where(
        ctr_noisy['previously_viewed'] > 0,
        0,
        ctr_noisy['previously_viewed'])

    ctr_noisy = ctr_noisy.groupby(['user_id', 'item_id'])['previously_viewed'].max().reset_index()

    ctr_viewed = viewed[viewed['previously_viewed'] >= 0][['user_id', 'item_id', 'step', 'viewed_position']]
    ctr_viewed = ctr_viewed.groupby(['user_id', 'item_id'])['step', 'viewed_position'].min().reset_index()

    # ctr_viewed is based on interraction
    # Problem on test, we don't have the last clickout
    # clicked is based on click out
    # Maybe there is a small bias
    # So remove viewed which happens but not followed by a click
    ctr_viewed = pd.merge(ctr_viewed, user_step_max)
    ctr_viewed = ctr_viewed[ctr_viewed['step'] <= ctr_viewed['user_step_max']].copy()

    ctr_viewed = pd.merge(ctr_viewed, clicked, how='left')
    ctr_viewed['target'] = ctr_viewed['target'].fillna(0)
    ctr_viewed = ctr_viewed.rename(columns={'target': 'ctr'})

    ctr_noisy = pd.merge(ctr_noisy, clicked, how='left')
    ctr_noisy['target'] = ctr_noisy['target'].fillna(0)
    ctr_noisy = ctr_noisy.rename(columns={'target': 'ctr_noisy'})

    ctr_viewed = pd.merge(user_fold, ctr_viewed)
    ctr_noisy = pd.merge(user_fold, ctr_noisy)

    def ctr_weighted(ctr_viewed, col_group):
        ctr_viewed['user_mean'] = ctr_viewed.groupby('user_id')['ctr'].transform('mean')
        ctr_viewed['user_weight'] = 1 / ctr_viewed['user_mean']
        ctr_viewed['ctr_weight'] = ctr_viewed['ctr'] * ctr_viewed['user_weight']
        agg_func = {
            'ctr_weight': ['sum', 'count'],
            'user_weight': ['sum'],
            'ctr': ['mean', 'count'],
            'viewed_position': ['mean'],
        }
        ctr_viewed.groupby('item_id').agg(agg_func)
        item_ctr = ctr_viewed.groupby(col_group).agg(agg_func)
        item_ctr.columns = ['_'.join(c) for c in item_ctr.columns]
        item_ctr = item_ctr.reset_index()
        item_ctr['ctr_weight_mean'] = item_ctr['ctr_weight_sum'] / item_ctr['user_weight_sum']
        item_ctr = item_ctr.drop(columns=['ctr_weight_sum', 'ctr_weight_count'])
        return item_ctr

    item_ctr = ml_utils.oof_dataframe(
        ctr_viewed,
        col_fold='user_fold',
        function=ctr_weighted,
        col_group=['item_id'],
        # target='ctr',
    )

    def fe_ctr_noisy(ctr_noisy, col_group):
        agg_func = {
            'ctr_noisy': ['mean', 'count'],
            'previously_viewed': ['mean'],
        }
        ctr_noisy.groupby('item_id').agg(agg_func)
        noisy = ctr_noisy.groupby(col_group).agg(agg_func)
        noisy.columns = ['_'.join(c) for c in noisy.columns]
        noisy = noisy.reset_index()
        return noisy

    item_ctr_noisy = ml_utils.oof_dataframe(
        ctr_noisy,
        col_fold='user_fold',
        function=fe_ctr_noisy,
        col_group=['item_id'],
        # target='ctr',
    )

    item_ctr = pd.merge(item_ctr_noisy, item_ctr, how='left')

    return user_fold, item_ctr


def get_fe_previous_interaction_prices(data):
    """
    For a given display :
    Get the rank (position) of the last action_type = (interaction or clickout)
    """
    print('TODO need refacto')
    is_interaction_or_click = (
        data['action_type'].map(constants.D_ACTION_TYPE)
        .isin(['interaction', 'clickout item']))
    cols = ['id', 'user_id', 'session_id', 'step', 'action_type', 'reference',
            'impressions', 'prices']
    prev_disp_inter = data[is_interaction_or_click][cols]

    impressions = utils.explode(prev_disp_inter[['id', 'user_id', 'session_id', 'step', 'reference', 'impressions']], 'impressions')
    impressions = impressions.rename(columns={'impressions': 'item_id'})

    # FE impressions
    impressions['position'] = 1
    grouped = impressions.groupby('id')
    impressions['position'] = grouped['position'].cumsum()
    # impressions_cnt = grouped['position'].size().rename('impressions_cnt').reset_index()
    # impressions = pd.merge(impressions, impressions_cnt)

    # Add price
    prices = utils.explode(prev_disp_inter[['id', 'user_id', 'session_id', 'prices']], 'prices')
    prices = prices.rename(columns={'prices': 'price'})
    prices['price'] = prices['price'].astype(int)
    prices['position'] = 1
    grouped = prices.groupby('id')
    prices['position'] = grouped['position'].cumsum()

    impressions = impressions[impressions['reference'] == impressions['item_id']]
    impressions = pd.merge(impressions, prices)

    impressions = impressions.drop_duplicates(['user_id', 'session_id', 'item_id'], keep='first')

    impressions = impressions.rename(columns={'price': 'interaction_price'})
    impressions['interaction_price_count'] = 1
    grouped = impressions.groupby(['user_id', 'session_id'])
    agg_func = {}
    agg_func['interaction_price'] = [
        'cumsum',
        'cummin', 'cummax',
        ]
    agg_func['interaction_price_count'] = ['cumsum']
    df_cum = grouped.agg(agg_func)
    df_cum.columns = ['_'.join(c) for c in df_cum.columns]
    df_cum['interaction_price_cummean'] = df_cum['interaction_price_cumsum'] / df_cum['interaction_price_count_cumsum']
    df_cum = df_cum.drop(columns='interaction_price_cumsum')

    res = pd.concat([impressions[['user_id', 'session_id', 'step', 'interaction_price']], df_cum], axis=1, sort=False)
    res['step'] += 1

    return res


def get_previous_interaction_cols_to_diff():
    """
    This function is defined just below get_fe_previous_interaction_prices()
    for understandability purpose.
    """
    previous_clickout_cols_to_diff = [
        'interaction_price',
        'interaction_price_cummin',
        'interaction_price_cummax',
        'interaction_price_cummean',
        ]
    return previous_clickout_cols_to_diff


def get_search(data):
    """
    Get the size (frequency) of ['city', 'search_type', 'search_reference']
    Intuition : Correlated to competiteness and listing quality
    """
    search = data[data['action_type'].map(constants.D_ACTION_TYPE) == 'search']
    search = search[['user_id', 'city', 'action_type', 'reference']].drop_duplicates()
    search = search.rename(
        columns={'action_type': 'search_type',
                 'reference': 'search_reference'})

    group_col = ['city', 'search_type', 'search_reference']
    group_name = '_&_'.join(c.upper() for c in group_col)
    search = search.groupby(group_col).size().rename(group_name + '_size').reset_index()

    # Old code for ideas, here just in case to keep ideas
    # Like proportion
    # First is there a search for poi, then is it a popular one or not
    # Maybe we should CountVectorizer on current_filters but problem on dimensions ...
    # poi = (data[data['action_type'] == 'search for poi'].rename(columns={'reference': 'poi'})
    #     .groupby(['city', 'poi'])
    #     .size().rename('count').reset_index())
    # poi['prop'] = poi['count'] / poi.groupby('city')['count'].transform('sum')
    # poi.sort_values('count')

    return search


def get_city(data):
    """
    Get the size (frequency) of ['city']
    Intuition : Correlated to competiteness and listing quality
    """
    city = data[['user_id', 'city']].drop_duplicates()
    group_col = ['city']
    group_name = '_&_'.join(c.upper() for c in group_col)
    city = city.groupby(group_col).size().rename(group_name + '_size').reset_index()
    return city


def get_fe_session(data):
    """
    For a given session :
    Get hour of the start of the session
    Label encode ['device', 'platform']
    """
    agg_func = {}
    agg_func['timestamp'] = ['min']

    n_session = data.session_id.nunique()

    fe_session = data.groupby(['session_id']).agg(agg_func)
    fe_session.columns = ['_'.join(col) for col in fe_session.columns]
    fe_session['datetime_min'] = pd.to_datetime(fe_session['timestamp_min'], unit='s')
    fe_session = ml_utils.date_fe(fe_session, 'datetime_min')
    fe_session = fe_session.drop(columns=['timestamp_min', 'datetime_min'])
    col_group = fe_session.index.name
    fe_session.columns = ['{}_{}'.format(col_group, col) for col in fe_session.columns]
    fe_session = fe_session.reset_index()

    sessions_info = data[['session_id', 'platform', 'city', 'device']].drop_duplicates()
    # Sometimes city changes ...
    sessions_info = sessions_info.drop_duplicates(['session_id'])
    fe_session = pd.merge(sessions_info, fe_session)
    assert n_session == fe_session.shape[0]

    # I'm a naughty boy, for production purpose should be done differently
    for col in ['device', 'platform']:
        le = sk.preprocessing.LabelEncoder()
        le.fit(fe_session[col])
        fe_session[col] = le.transform(fe_session[col])

    fe_session = fe_session.drop(columns=['city'])

    return fe_session


def get_fe_user_item_current_session(data):
    """
    For a given (user_id, item_id, session_id) get from the previous step:
        last timestamp (TODO rename previous) a user interacted with item_id during the session
        count of interactions with item_id since start of session (how much time a user interacted with an item_id)
    """

    # Part last interaction -----------------------------------------------
    # last interaction can be a click out ...
    key = ['user_id', 'session_id', 'reference']
    col = ['action_type']
    col_group = key + ['step']
    df_fe = data[key + ['step', 'timestamp'] + col].copy()
    df_fe = df_fe[df_fe.reference.notnull()]
    df_fe['interaction_cnt'] = 1
    grouped = df_fe.groupby(key)
    df_fe['interaction_cnt'] = grouped['interaction_cnt'].cumsum()
    df_fe['step_last'] = df_fe['step']

    for col in ['action_type']:
        le = sk.preprocessing.LabelEncoder()
        le.fit(df_fe[col])
        df_fe[col] = le.transform(df_fe[col])

    # Avoid leak, this information is only available one step after
    df_fe['step'] += 1

    df_fe = df_fe.set_index(col_group).add_prefix('session_previous_interaction_').reset_index()
    df_fe = df_fe.rename(columns={'reference': 'item_id'})
    return df_fe


def get_fe_user_current_session(data):
    """
    Same as previous but not at item granularity
    """

    # Part last interaction -----------------------------------------------
    # last interaction can be a click out ...
    key = ['user_id', 'session_id']
    col = ['action_type']
    col_group = key + ['step']
    df_fe = data[data.reference.notnull()][key + ['step', 'timestamp'] + col].copy()
    df_fe['interaction_cnt'] = 1
    grouped = df_fe.groupby(key)
    df_fe['interaction_cnt'] = grouped['interaction_cnt'].cumsum()
    # df_fe['step_last'] = df_fe['step']

    for col in ['action_type']:
        le = sk.preprocessing.LabelEncoder()
        le.fit(df_fe[col])
        df_fe[col] = le.transform(df_fe[col])

    # Avoid leak, this information is only available one step after
    df_fe['step'] += 1

    df_fe = df_fe.set_index(col_group).add_prefix('interaction_').reset_index()
    # df_fe = df_fe.rename(columns={'reference': 'item_id'})
    return df_fe

#
# def get_fe_user_current_session(fe_user_item_current_session):
#     """
#     Not a item granularity
#     """
#     fe_user_current_session = fe_user_item_current_session.copy()
#     fe_user_current_session = fe_user_current_session.drop(columns=['item_id', 'session_previous_interaction_step_last'])
#     fe_user_current_session.columns = [s.replace('session_previous_', '') for s in fe_user_current_session.columns]
#     return fe_user_current_session


def get_clickout(data):
    """
    Informations about the previous clickout
    Returns step and timestamp of the previous clickout.
    We want to know if the lats interaction with an item was a clickout or not.
    If yes, it is like resetting the "game"
    """
    cond = data['action_type'] == 'clickout item'
    key = ['user_id', 'session_id']
    clikckout = data[cond][key + ['step', 'timestamp']].copy()
    clikckout = clikckout.rename(columns={'timestamp': 'previous_clickout_timestamp'})
    clikckout['previous_clickout_step'] = clikckout['step']
    clikckout['step'] += 1
    return clikckout


def get_previous_clickout(data):
    """
    Was an item_id previously clicked and when
    """
    cond = data['action_type'] == 'clickout item'
    key = ['user_id', 'session_id', 'reference']
    clikckout = data[cond][key + ['step', 'timestamp']].copy()
    clikckout = clikckout.rename(columns={'timestamp': 'item_clickout_timestamp'})
    clikckout['previous_clickout_step'] = clikckout['step']
    clikckout['step'] += 1
    return clikckout



def get_fe_user_item_previous_session(data):
    """
    For a given (user_id, item_id, session_cnt) get from the previous session:
        last timestamp (TODO rename previous) with item_id
        count of interactions with item_id
    """
    # fe_user_session_item_id = get_fe_key(data, key=['user_id', 'reference'])
    key = ['user_id', 'reference']
    col_timewise = 'session_cnt'
    col_group = key + [col_timewise]
    fe_user_item_previous_session = data[key + [col_timewise, 'timestamp']].copy()
    # Uncomment this once bug is found
    # fe_user_item_id = fe_user_item_id[fe_user_item_id.reference.notnull()]
    fe_user_item_previous_session['interaction_cnt'] = 1
    grouped = fe_user_item_previous_session.groupby(key)
    fe_user_item_previous_session['interaction_cnt'] = grouped['interaction_cnt'].cumsum()

    fe_user_item_previous_session['timestamp'] = fe_user_item_previous_session['timestamp']

    # Avoid leak, this information is only available for the next session
    fe_user_item_previous_session[col_timewise] += 1

    fe_user_item_previous_session = (
        fe_user_item_previous_session
        .set_index(col_group)
        .add_prefix('previous_session_previous_interaction_')
        .reset_index())

    fe_user_item_previous_session = fe_user_item_previous_session.rename(columns={'reference': 'item_id'})
    return fe_user_item_previous_session


def get_session_search_timestamp(data):
    """
    Simply get timestamp of search
    Then make a rolling join with df_learning and compute diff between
    timestamp of the clickout and timestamp_search
    Intuition :
        If click is fast, most likely on item ranked high
        If click slow, user probably scrolled down, and clicked on a lower ranked impressions
    """
    session_search_timestamp = data['action_type'].map(constants.D_ACTION_TYPE).isin(['search', 'change of sort order'])
    session_search_timestamp = data[session_search_timestamp].copy()
    session_search_timestamp = session_search_timestamp[['user_id', 'session_id', 'timestamp']]
    session_search_timestamp['timestamp_search'] = session_search_timestamp['timestamp']  # Used later on for rolling join
    return session_search_timestamp


def get_fe_previous_interaction_display(data):
    """
    For a given display :
    Get the rank (position) of the last action_type = (interaction or clickout)
    """
    is_interaction_or_click = (
        data['action_type'].map(constants.D_ACTION_TYPE)
        .isin(['interaction', 'clickout item']))
    cols = ['id', 'user_id', 'session_id', 'step', 'action_type', 'reference',
            'impressions']
    prev_disp_inter = data[is_interaction_or_click][cols]

    impressions = (
        utils.explode(
            prev_disp_inter[['impressions']]
            .drop_duplicates().eval('reference = impressions'), 'reference'))
    impressions['position'] = (
        impressions.eval('position = 1')
        .groupby('impressions')['position'].cumsum())
    prev_disp_inter = pd.merge(prev_disp_inter, impressions, how='left')

    prev_disp_inter = prev_disp_inter[
        ['user_id', 'session_id', 'impressions', 'step', 'position']]
    # Now shift
    prev_disp_inter['step'] += 1
    prev_disp_inter = prev_disp_inter.rename(columns={'position': 'prev_disp_inter_position'})
    prev_disp_inter = prev_disp_inter.drop_duplicates(['user_id', 'session_id', 'step'])  # See why
    return prev_disp_inter, impressions


def get_fe_display(data):
    """
    Fe about a display (a list of impressions)
    """
    prev_disp_inter, _ = get_fe_previous_interaction_display(data)
    fe_display = prev_disp_inter

    return fe_display


def get_viewed(data):
    """
    Was an item_id previously viewed.
    If viewed but there is no previous interaction, means user decided to not click.
    Implicit negative feedback.
    """
    previous_interaction, impressions = get_fe_previous_interaction_display(data)
    impressions = impressions.rename(columns={'interaction_position': 'position'})
    # Max position and step, only available when display changes.

    viewed = (
        previous_interaction
        .groupby(['user_id', 'session_id', 'impressions'])[
            'prev_disp_inter_position', 'step'].max().reset_index())

    viewed = viewed.rename(columns={'prev_disp_inter_position': 'max_interaction_position'})
    viewed = pd.merge(impressions, viewed)
    # previously_viewed >= 0 means viewed for sure
    # -1 not sure, -2 even less sure, and so on
    viewed['previously_viewed'] = (
        viewed['max_interaction_position'] - viewed['position'])
    viewed = viewed[['user_id', 'session_id', 'step', 'reference',
                     'previously_viewed', 'position']]
    viewed = viewed.rename(columns={
        'reference': 'item_id',
        'position': 'viewed_position',
        })

    # Don't shift, because already shifted in get_fe_previous_interaction_display()
    # viewed['step'] += 1

    return viewed


def get_fe_previous_clickout(display, item):
    """
    Diverse FE about previous clickout

    Nb : display was exploded and contains price, we are not really interested about display info.
    This is not FE about display, which can be misleading.
    """
    cols = ['user_id', 'session_id', 'step', 'reference', 'price']
    df = display[display['target'] == 1][cols].copy()
    df = df.rename(columns={'price': 'previous_price'})
    df['previous_price_count'] = 1
    grouped = df.groupby(['user_id', 'session_id'])
    agg_func = {}
    agg_func['previous_price'] = [
        'cumsum',
        'cummin', 'cummax',
        ]
    agg_func['previous_price_count'] = ['cumsum']
    df_cum = grouped.agg(agg_func)
    df_cum.columns = ['_'.join(c) for c in df_cum.columns]
    df_cum['previous_price_cummean'] = df_cum['previous_price_cumsum'] / df_cum['previous_price_count_cumsum']
    df_cum = df_cum.drop(columns='previous_price_cumsum')
    df = df.drop(columns='previous_price_count')
    df = pd.concat([df, df_cum], axis=1)
    df = df.rename(columns={'reference': 'previous_item_id'})
    df = pd.merge(
            df,
            item[['item_id', 'properties']].add_prefix('previous_'),
            how='left',
            )

    # Shift to avoid leak
    df['step'] += 1
    return df


def get_previous_clickout_cols_to_diff():
    """
    This function is defined just below get_fe_previous_clickout()
    for understandability purpose.
    """
    previous_clickout_cols_to_diff = [
        'previous_price',
        'previous_price_cummin',
        'previous_price_cummax',
        'previous_price_cummean',
        ]
    return previous_clickout_cols_to_diff


def get_fe_city_platform(data, display):
    """
    Should rename function
    Poor target about price and position for the first clickout
    """
    cond = data.action_type == 'clickout item'
    clicked = data[cond][['id', 'platform', 'city', 'device']].drop_duplicates(['id'])
    clicked['country'] = clicked['city'].str.split(', ').str[1]
    K_FOLD = 20
    clicked['fold'] = np.random.randint(K_FOLD, size=clicked.shape[0])
    cond_clicked = display['target'] == 1
    c2 = display['clickout_cumsum'] == 1
    city_platform = pd.merge(
        clicked,
        display[cond_clicked & c2][['id', 'item_id', 'price', 'position']])
    city_platform = city_platform.drop_duplicates('id').copy()

    print('WIP')
    # user_is_local = city_platform.copy()
    # platform = city_platform.groupby('platform').size().rename('platform_size').reset_index()
    # country = city_platform.groupby('country').size().rename('country_size').reset_index()
    # city = city_platform.groupby('city').size().rename('city_size').reset_index()
    # user_is_local = user_is_local.groupby(['platform', 'country'])
    # user_is_local = user_is_local.size().rename('country_platform_count').reset_index()
    # user_is_local['country_platform_count_max'] = user_is_local.groupby('country')['country_platform_count'].transform('max')
    # user_is_local['country_count'] = user_is_local.groupby('country')['country_platform_count'].transform('sum')
    # user_is_local['user_is_local'] = user_is_local['country_platform_count_max'] == user_is_local['country_count']
    # user_is_local[user_is_local['user_is_local']].sort_values('country_platform_count')
    # user_is_local['country_platform_prop'] = user_is_local['country_platform_count'] / user_is_local['platform_count']
    # user_is_local['platform_local_prop'] = user_is_local['country_platform_count_max'] / user_is_local['platform_count']
    # user_is_local = user_is_local.drop(columns='country_platform_count_max')
    # user_is_local['country_platform_count_max'] = user_is_local.groupby('platform')['country_platform_count'].transform('max')
    # user_is_local['platform_count'] = user_is_local.groupby('platform')['country_platform_count'].transform('sum')
    # user_is_local['user_is_local'] = user_is_local['country_platform_count_max'] == user_is_local['country_platform_count']
    # user_is_local['country_platform_prop'] = user_is_local['country_platform_count'] / user_is_local['platform_count']
    # user_is_local['platform_local_prop'] = user_is_local['country_platform_count_max'] / user_is_local['platform_count']
    # user_is_local = user_is_local.drop(columns='country_platform_count_max')

    df_fe = clicked[['id', 'platform', 'city', 'country', 'device', 'fold']].drop_duplicates(['id', 'fold'])
    # df_fe = clicked[['id', 'fold']].drop_duplicates()
    cols_group = [
        ['platform', 'country'],
        ['platform', 'city'],
        ['platform', 'device'],
        ['device'],
        ]
    for col_group in cols_group:
        for t in ['price', 'position']:
            df_agg = ml_utils.oof_dataframe(
                city_platform,
                'fold',
                ml_utils.target_embedding,
                col=col_group,
                target=t,
                )

            if t != 'price':
                # Do not keep multiple count columns
                df_agg = df_agg.iloc[:, ~df_agg.columns.str.endswith('_count')]

            # df_agg = df_agg.drop(columns=col_group)
            nrow = df_fe.shape[0]
            df_fe = pd.merge(
                df_fe,
                df_agg, how='left')
            assert df_fe.shape[0] == nrow

    df_fe = df_fe.drop(columns=['platform', 'city', 'country', 'device'])

    return df_fe


def get_learning(display,
                 item,
                 city,
                 search,
                 fe_session,
                 fe_display,
                 fe_user_item_current_session,
                 fe_user_current_session,
                 fe_user_item_previous_session,
                 clickout,
                 session_search_timestamp,
                 fe_previous_clickout,
                 fe_city_platform,
                 viewed,
                 user_fold,
                 item_ctr,
                 previous_interaction_prices,
                 fe_item_position,
                 ):
    """
    Join different dataset to obtain df_learning
    display = base dataset
    """
    nrow_begin = display.shape[0]

    df_learning = display

    df_learning = pd.merge(df_learning, item, how='left')

    df_learning = pd.merge(df_learning, search, how='left')
    df_learning = pd.merge(df_learning, city, how='left')
    df_learning = pd.merge(df_learning, fe_session)
    df_learning = pd.merge(df_learning, fe_display, how='left')
    df_learning = pd.merge(df_learning, fe_city_platform, how='left')
    df_learning = pd.merge(df_learning, user_fold, how='left')
    df_learning = pd.merge(df_learning, item_ctr, how='left')
    df_learning = pd.merge(df_learning, fe_item_position, how='left')

    dict_merge_asof = {}

    dict_merge_asof['fe_user_item_current_session'] = {
        'df_to_merge': fe_user_item_current_session,
        'col_timewise': 'step',
    }

    dict_merge_asof['fe_user_current_session'] = {
        'df_to_merge': fe_user_current_session,
        'col_timewise': 'step',
    }

    dict_merge_asof['fe_user_item_previous_session'] = {
        'df_to_merge': fe_user_item_previous_session,
        'col_timewise': 'session_cnt',
    }

    dict_merge_asof['clickout'] = {
        'df_to_merge': clickout,
        'col_timewise': 'step',
    }

    dict_merge_asof['session_search_timestamp'] = {
        'df_to_merge': session_search_timestamp,
        'col_timewise': 'timestamp',
    }

    dict_merge_asof['fe_previous_clickout'] = {
        'df_to_merge': fe_previous_clickout,
        'col_timewise': 'step',
    }

    dict_merge_asof['viewed'] = {
        'df_to_merge': viewed,
        'col_timewise': 'step',
    }

    dict_merge_asof['previous_interaction_prices'] = {
        'df_to_merge': previous_interaction_prices,
        'col_timewise': 'step',
    }

    for merge_name, config in dict_merge_asof.items():
        print('Merge asof : {}'.format(merge_name))
        col_timewise = config['col_timewise']
        df_to_merge = config['df_to_merge']
        by = list(df_learning.columns.intersection(df_to_merge.columns))
        by.remove(col_timewise)
        df_learning = df_learning.sort_values(col_timewise)
        df_to_merge = df_to_merge.sort_values(col_timewise)

        df_learning = pd.merge_asof(
            df_learning,
            df_to_merge,
            on=col_timewise,
            by=by)

    nrow_end = df_learning.shape[0]
    assert nrow_begin == nrow_end

    return df_learning


def fe_learning(df_learning):
    """
    Some features FE performed here are independants of other dataset (eg price rank)
    Might be done before.
    """
    df_learning['session_previous_interaction_timestamp_diff'] = df_learning['timestamp'] - df_learning['session_previous_interaction_timestamp']
    df_learning['interaction_timestamp_diff'] = df_learning['timestamp'] - df_learning['interaction_timestamp']

    df_learning['search_timestamp_diff'] = df_learning['timestamp'] - df_learning['timestamp_search']
    df_learning['position_prev_disp_inter_position_diff'] = df_learning['position'] - df_learning['prev_disp_inter_position']

    df_learning['position_diff_timestamp_diff_rt'] = (
            df_learning['position_prev_disp_inter_position_diff'] / df_learning['interaction_timestamp_diff'])

    df_learning['interaction_interaction_cnt_Clickout_cumsum_rt'] = (
        (df_learning['interaction_interaction_cnt'].fillna(0) + 1) / df_learning['clickout_cumsum'])

    # Compute position differences with position target embedding
    te_cols_position = [
        'platform_country_position_mean',
        'platform_city_position_mean',
        'platform_device_position_mean',
        'device_position_mean',
        ]

    for c in te_cols_position:
        df_learning['{}_diff'.format(c)] = (
            df_learning['position'] - df_learning[c])

    # Compute various price differences
    cols_to_diff = get_previous_clickout_cols_to_diff() + get_previous_interaction_cols_to_diff()
    for col in cols_to_diff:
        df_learning['price_{}_log_diff'.format(col)] = (
            np.log1p(df_learning['price']) - np.log1p(df_learning[col]))

    df_learning = df_learning.drop(columns=cols_to_diff)  # Avoid overfitting, discard absolute values

    df_learning['previous_clickout_step_diff'] = df_learning['step'] - df_learning['previous_clickout_step']
    df_learning['previous_clickout_timestamp_diff'] = df_learning['timestamp'] - df_learning['previous_clickout_timestamp']

    grouped = df_learning.groupby('id')
    to_rank_ascending = ['price']
    # Warning by default ascending = True
    for col in to_rank_ascending:
        df_learning['{}_rank'.format(col)] = grouped[col].rank()

    to_rank_descending = [
        'session_previous_interaction_interaction_cnt',
        'session_previous_interaction_timestamp_diff',
        'position_prev_disp_inter_position_diff',
        'previous_session_previous_interaction_interaction_cnt',
        # 'properties_cnt',
        ]

    # Add item_id_ctr_mean once debugged

    for col in to_rank_descending:
        df_learning['{}_rank'.format(col)] = grouped[col].rank(
            ascending=False,
            na_option='bottom',
            method='min',
            )

    # Doesn't seem usefull, therefore reduce memory
    to_scale_minmax = ['price']

    for col in to_scale_minmax:
        df_learning['{}_minmax_scaled'.format(col)] = (
            (df_learning[col] - grouped[col].transform('min')) /
            grouped[col].transform('max'))

    # df_learning['price_log'] = np.log(df_learning['price'])
    # , 'price_log'

    to_lead_lag = ['price']
    for col in to_lead_lag:
        for i in [-2, -1, 1, 2]:
            df_learning['{}_{}_diff'.format(col, i)] = df_learning[col] - grouped[col].transform('shift', i)

    return df_learning


def get_X_X_cols(df, X_cols_dense, tfidf_vectorizer):
    X_cols_sparse = tfidf_vectorizer.get_feature_names()
    X_cols_sparse = ['properties_' + i for i in X_cols_sparse]
    X = df[X_cols_dense].copy()
    # print('tfidf_vectorizer')
    X_sparse = tfidf_vectorizer.transform(df['properties'].fillna(''))
    # X_sparse_previous = tfidf_vectorizer.transform(df['previous_properties'].fillna(''))
    # previous_similarity = X_sparse.multiply(X_sparse_previous).sum(axis=1)
    # X['previous_similarity'] = previous_similarity

    # print('hstack')
    SIZE_CHUNK = 500000
    X, X_cols = ml_utils.hstack_dense_sparse(
        X,
        X_sparse,
        X_cols_sparse,
        SIZE_CHUNK=SIZE_CHUNK,
        )
    return X, X_cols


def clean_data_quality_problem(df_learning):
    """
    For some reasons user_id, session_id, step is not unique on rare case
    Remove them for security/sanity
    """
    anormal = (df_learning.groupby(['id', 'session_id'])['target'].sum())
    anormal = anormal[anormal != 1]
    anormal = anormal.reset_index()['session_id']
    df_learning = df_learning[~df_learning['session_id'].isin(anormal)]

    return df_learning


def get_sub(df_test, test):
    key = ["user_id", "session_id", "timestamp", "step"]
    df_test = df_test.sort_values(key + ["pred"], ascending=[True, True, True, True, False])
    df_test['item_id'] = df_test['item_id'].astype(str)
    submission = df_test.groupby(key)['item_id'].apply(lambda x: ' '.join(x)).reset_index()
    submission = pd.merge(test[key], submission)
    submission = submission.rename(columns={'item_id': 'item_recommendations'})
    return submission
