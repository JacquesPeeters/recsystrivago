import pandas as pd


def assert_test_clickout_is_last(test):
    """
    Assert/test that we are asked to predict the last clikout item available from a session ;)
    So no leak
    """
    tmp = test[test['action_type'] == 'clickout item'].copy()
    tmp = pd.merge(tmp, tmp.groupby('session_id')['step'].max().rename('step_max').reset_index())
    tmp = tmp[tmp['reference'].isnull()].copy()
    assert (tmp['step'] == tmp['step_max']).all()
    print('Check passed')
    return None


def verify_step1_na_for_some_cols(df_learning):
    """
    Asssert that for step 1 we have no value for previous step
    """
    step_1 = df_learning['step'] == 1
    should_null = df_learning['user_id_session_id_reference_step_interaction_cnt'].notnull()
    assert df_learning[step_1 & should_null].shape[0] == 0
    print('user_id_session_id_reference_step_interaction_cnt always null at step 0: Success')
    return None
